import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';


const Notification = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image style={styles.topimg} source={require('./assets/UI_design_top.png')} />
                <Text style={{ textAlign: "right", marginTop: -20, paddingRight: 20, fontSize: 30, }}>Notifications</Text>
                <Text style={{ paddingTop: 30, paddingLeft: 10 }}>Mark all read                                                       Sort by Time</Text>

            </View>
            <View style={styles.container2}>

                <View style={styles.box}>
                    <View style={styles.inner}>
                        <Image style={styles.img1} source={require('./assets/fred.jpg')} />
                        <Text style={{ marginTop: 30, fontWeight: "bold", }}>Frederick Marlang{"\n"}Commented on your post.</Text>
                        <Image style={styles.img2} source={require('./assets/Elipsis.png')} />
                        <Text style={styles.time}>2m ago.</Text>
                    </View>

                </View>
                <View style={styles.box}>
                    <View style={styles.inner1}>
                        <Image style={styles.img1} source={require('./assets/sai.jpg')} />
                        <Text style={{ marginTop: 30 }}>Saira Caga{"\n"}Mentioned you in a comment.</Text>
                        <Image style={styles.img2} source={require('./assets/Elipsis.png')} />
                        <Text style={styles.time}>5m ago.</Text>
                    </View>
                </View>
                <View style={styles.box}>
                    <View style={styles.inner1}>
                        <Image style={styles.img1} source={require('./assets/ore.jpg')} />
                        <Text style={{ marginTop: 30 }}>Jon Arvy Enriquez{"\n"}Liked your photo.</Text>
                        <Image style={styles.img2} source={require('./assets/Elipsis.png')} />
                        <Text style={styles.time}>3h ago.</Text>
                    </View>
                </View>
                <View style={styles.box}>
                    <View style={styles.inner1}>
                        <Image style={styles.img1} source={require('./assets/marialy.jpg')} />
                        <Text style={{ marginTop: 30 }}>Marialy Detondoy{"\n"}Shared your post.</Text>
                        <Image style={styles.img2} source={require('./assets/Elipsis.png')} />
                        <Text style={styles.time}>5h ago.</Text>
                    </View>
                </View>
                <View style={styles.box}>
                    <View style={styles.inner1}>
                        <Image style={styles.img1} source={require('./assets/christian.jpg')} />
                        <Text style={{ marginTop: 30 }}>Christian Angelo Navera{"\n"}Commented on your post.</Text>
                        <Image style={styles.img2} source={require('./assets/Elipsis.png')} />
                        <Text style={styles.time}>7h ago.</Text>
                    </View>
                </View>
            </View>
            <View style={styles.navbar}>
                <View style={styles.navcontainer}>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/home_or_newsfeed.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/new_notification.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/create_post_button.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/new_message.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/search_icon.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',


    },
    header: {
        flexDirection: 'column',
        backgroundColor: "#FFFF",
        height: '20%',
        width: '100%',

    },
    container2: {

        width: '100%',
        height: '25%',
        padding: 3,
        flexDirection: "row",
        flexWrap: 'wrap',
        marginTop: 20,
    },
    box: {
        width: '100%',
        height: '50%',

    },
    inner: {
        flex: 1,
        backgroundColor: "#F5F5F5",
        flexDirection: "row",
    },
    inner1: {
        flex: 1,
        backgroundColor: "#FFFF",
        flexDirection: "row",
        borderBottomWidth: 1,
        borderColor: "#DCDCDC"
    },

    img1: {
        marginRight: 7,
        marginTop: 20,
        marginLeft: 25,
        width: 65,
        height: 65,
        borderRadius: 100,
    },
    img2: {

        width: 25,
        height: 25,
        marginTop: 25,
        marginRight: 40,
        position: "absolute",
        right: 0,
    },
    time: {
        marginTop: 47,
        marginLeft: 0,
        marginRight: 26,
        fontSize: 13,
        position: "absolute",
        right: 0,
    },
    navbar: {
        position: "absolute",
        bottom: 0,


    },
    navcontainer: {
        backgroundColor: "rgba(255, 255, 255, 0.3)",
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
        bottom: 0,
    },
    navbutton: {
        alignItems: "center",
        justifyContent: "center",
    },
    navicon: {
        width: 35,
        height: 35,
        marginHorizontal: 20,
    },
});
export default Notification;