import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Dimensions, ScrollView, ActivityIndicator } from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class API extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: null,
            subReddit: 'Shadowverse',
            postCount: 50,
            authorData: null,
        }
    }

    componentDidMount() {
        return fetch('https://meme-api.herokuapp.com/gimme/Shadowverse/50')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.memes,
                })
            })
            .catch((error) => {
                console.log(error)
            });


    }

    validate() {
        console.log(this.state.subReddit);

        if (this.state.postCount > 50 || this.state.postCount < 1) {
            return;
        } else {
            this.setState({
                isLoading: true,
            });
            return fetch('https://meme-api.herokuapp.com/gimme/' + this.state.subReddit + '/' + this.state.postCount)
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        isLoading: false,
                        dataSource: responseJson.memes,
                    })
                })
                .catch((error) => {
                    console.log(error)
                });
        }
    }

    fetchUserImages(author) {

        //console.log('https://www.reddit.com/user/' + author + '/about.json');
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={[styles.container, { flex: 1, alignItems: 'center', justifyContent: 'center', }]}>
                    <Image style={{ width: 70, height: 70 }} source={{ uri: 'https://c.tenor.com/0iK9a1WkT40AAAAC/loading-white.gif' }} />
                    <ActivityIndicator />
                </View>
            );
        } else {

            let content = this.state.dataSource.map((val, key) => {
                return <View key={key} style={styles.postcontainer}>
                    {this.fetchUserImages(val.author)}
                    <View style={styles.rowarrange}>
                        <TouchableOpacity style={[styles.friendsbutton, { marginLeft: 25, marginTop: 10, marginBottom: 5, }]}>
                            <Image style={styles.profilepic} source={{ uri: 'https://apkvision.com/wp-content/uploads/2017/09/6896.jpg' }} />
                        </TouchableOpacity>
                        <View >
                            <Text style={{ marginTop: 16, fontWeight: 'bold' }}>{val.subreddit}</Text>
                            <Text>{val.author}</Text>
                        </View>
                    </View>
                    <Text style={styles.caption}>{val.title}</Text>
                    <View >
                        <TouchableOpacity style={[styles.postimgbtn, { backgroundColor: '#f2f2f2' }]}>
                            <Image style={styles.postimg} source={{
                                uri: `${val.url}`
                            }} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.besideoptions}>
                        <TouchableOpacity style={[styles.besideiconbutton, { marginTop: 20, marginBottom: 50 }]}>
                            <Image style={styles.besideicon} source={require('./assets/option.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.besideiconbutton}>
                            <Image style={styles.besideicon} source={require('./assets/heart.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.besideiconbutton}>
                            <Image style={styles.besideicon} source={require('./assets/comment.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.besideiconbutton}>
                            <Image style={styles.besideicon} source={require('./assets/share.png')} />
                        </TouchableOpacity>
                    </View>
                </View >
            });

            return (
                <View style={styles.container}>

                    <Text style={{ marginLeft: 30, fontSize: 40 }}>iKonek x Reddit</Text>
                    <View style={styles.rowarrange}>
                        <TextInput style={[styles.textinput, { width: 190, marginLeft: 10, }]} returnKeyLabel={"next"} onChangeText={(text) => this.setState({ subReddit: text })} placeholder="Subreddit" />
                        <TextInput style={styles.textinput} returnKeyLabel={"next"} onChangeText={(text) => this.setState({ postCount: text })} placeholder="Post Count" />
                        <TouchableOpacity style={styles.buttonstyle} onPress={() => this.validate()}>
                            <Text style={styles.buttontext}>Search</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ height: 615 }}>
                        <ScrollView style={styles.postsscrollview}>
                            {content}
                        </ScrollView>
                    </View>
                </View>
            );
        }

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        height: windowHeight,
        width: windowWidth,
    },
    topimg: {
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginTop: 0,
    },
    bottomimg: {
        position: "absolute",
        bottom: 0,
        justifyContent: "flex-end",
        alignSelf: "flex-end",
        marginTop: 10,
    },

    rowarrange: {
        flexDirection: "row",
    },

    friendsbar: {
        flexDirection: "row",
        marginLeft: 30,
        marginBottom: 5,
    },

    friendsbutton: {
        alignItems: "center",
        justifyContent: "center",
        marginRight: 10,
    },

    navbar: {
        position: "absolute",
        bottom: 0,

    },
    navcontainer: {
        backgroundColor: "rgba(255, 255, 255, 0.3)",
        width: windowWidth,
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    navbutton: {
        alignItems: "center",
        justifyContent: "center",
    },
    navicon: {
        width: 35,
        height: 35,
        marginHorizontal: 20,
    },
    userprofile: {
        position: "absolute",
        right: 20,
        top: 60,
    },
    profilepic: {
        width: 50,
        height: 50,
        borderRadius: 100,
    },

    textinput: {
        fontSize: 14,
        textAlign: "center",
        backgroundColor: "#f5f5f5",
        width: 100,
        height: 40,
        marginLeft: 5,
        borderRadius: 10,
    },

    postcontainer: {
        backgroundColor: "#fff",
        marginTop: 7,
        paddingBottom: 15,
        borderRadius: 10,
    },

    postimgbtn: {
        marginLeft: 25,
        width: 300,
        height: null,
    },

    postimg: {
        width: 300,
        height: 200,
        resizeMode: "contain",
    },

    besideoptions: {
        width: 67,
        height: 270,
        position: "absolute",
        right: 0,
    },
    besideiconbutton: {
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 10,
    },

    besideicon: {
        width: 35,
        height: 35,

    },

    caption: {
        marginLeft: 25,
        marginBottom: 5,
        width: 300,
        fontWeight: 'bold',
    },

    buttonstyle: {
        backgroundColor: "#02abff",
        width: 70,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,

        borderWidth: 1,
        borderColor: "#0200ff",
        borderRadius: 10,
    },

    postsscrollview: {
        height: 200,
        backgroundColor: "#f6f5e7",
    },
});