import React from 'react';
import { StyleSheet, Text, View, Image, Button, SafeAreaView, TouchableOpacity, TextInput } from 'react-native';



const Status = (props) => {
    return (
        <View style={styles.container}>
            <Image style={styles.topimg} source={require('./assets/UI_design_top.png')} />
            <Text style={{ textAlign: "right", marginRight: 20, marginTop: -10, fontSize: 30, fontWeight: "bold" }}>STATUS</Text>


            <View style={{ flexDirection: "row" }}>
                <Image style={[styles.avatar, { marginLeft: 50 }]} source={require('./assets/sai.jpg')} />
                <Text style={{ marginTop: 13, fontWeight: "bold" }}>Saira Mae Caga</Text>
            </View>
            <Text style={styles.caption}>Life is like an Array. Always starts at Zero</Text>
            <View style={{ alignItems: "center", justifyContent: "center", marginBottom: 30, }}>
                <TouchableOpacity>
                    <Image style={{ width: 300, height: 200 }} source={require('./assets/monalisa.jpg')} />
                </TouchableOpacity>
            </View>


            <View style={{ flexDirection: "row" }}>
                <View style={{ flexDirection: "row", marginLeft: 25, }}>
                    <TouchableOpacity style={styles.belowicons}>
                        <Image style={styles.belowicons} source={require('./assets/camera.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.belowicons}>
                        <Image style={styles.belowicons} source={require('./assets/emoticon.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.belowicons}>
                        <Image style={styles.belowicons} source={require('./assets/tag.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.belowicons}>
                        <Image style={styles.belowicons} source={require('./assets/otheropt.png')} />
                    </TouchableOpacity>
                </View>
                <View style={styles.postcontainer}>
                    <TouchableOpacity
                        style={styles.buttonstyle}
                        onPress={() => console.log("POST")}>
                        <Text style={styles.buttontext}>POST</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.navbar}>
                <View style={styles.navcontainer}>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/home_or_newsfeed.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton} onPress={() => navigation.navigate('Notification')}>
                        <Image style={styles.navicon} source={require('./assets/new_notification.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton} onPress={() => navigation.navigate('Status')}>
                        <Image style={styles.navicon} source={require('./assets/create_post_button.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton} onPress={() => navigation.navigate('Inbox')}>
                        <Image style={styles.navicon} source={require('./assets/new_message.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navbutton}>
                        <Image style={styles.navicon} source={require('./assets/search_icon.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>


    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },

    header: {
        flexDirection: 'column',
        backgroundColor: "#FFFF",
        height: '20%',
        width: '100%',
    },
    navbar: {
        position: "absolute",
        bottom: 0,

    },
    navcontainer: {
        backgroundColor: "rgba(255, 255, 255, 0.3)",
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    navbutton: {
        alignItems: "center",
        justifyContent: "center",
    },
    navicon: {
        width: 35,
        height: 35,
        marginHorizontal: 20,
    },
    // container: {
    //     width: '100%',
    //     height: '25%',
    //     padding: 3,
    //     flexDirection: "row",
    //     flexWrap: 'wrap',
    // },

    topimg: {
        marginTop: -3,
        marginLeft: -3,
    },
    img: {
        width: 60,
        height: 60,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 10,
        resizeMode: 'cover',
        borderWidth: 2,
        borderRadius: 75,

    },
    caption: {
        marginLeft: 50,
        marginVertical: 20,
        width: 200,

    },
    // postcontainer: {
    //     backgroundColor: "#fff",
    //     marginTop: 7,
    //     paddingBottom: 15,
    // },
    postcontainer: {
        flexDirection: "row",
        marginHorizontal: 20,
        marginTop: 0,
        marginBottom: 0,

        alignItems: "flex-end",
        justifyContent: "center",
    },
    avatar: {
        width: 48,
        height: 48,
        borderRadius: 24,
        marginRight: 16
    },

    buttonstyle: {
        backgroundColor: "#02abff",
        width: 70,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 30,
        marginRight: 5,
        marginTop: 12,

        borderWidth: 1,
        borderColor: "#0200ff",
        borderRadius: 10,
    },

    buttontext: {
        color: "#ffffff"
    },

    belowicons: {
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginTop: 10,
        marginHorizontal: 10,
        width: 30,
        height: 30,

    },


});
export default Status;