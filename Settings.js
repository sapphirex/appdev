import React from 'react';
import { StyleSheet, Text, View, Image, Button, SafeAreaView, TouchableOpacity, TextInput, Dimensions } from 'react-native';
import { useFonts } from 'expo-font';
import API from './API';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Tab = createNativeStackNavigator();

function Settings(props) {
    const [loaded] = useFonts({
        GlacialIndifference: require('./assets/fonts/GlacialIndifference-Regular.otf'),
        GlacialIndifferenceItalic: require('./assets/fonts/GlacialIndifference-Italic.otf'),
        GlacialIndifferenceBold: require('./assets/fonts/GlacialIndifference-Bold.otf'),
    });

    if (!loaded) {
        console.log("null");
    }

    return (
        <Tab.Navigator>
            <Tab.Screen component={SettingsNavigation} name="SettingsNavigation" options={{ headerShown: false }} />
            <Tab.Screen component={API} name="API" options={{ headerShown: false }} />
        </Tab.Navigator>
    );
}



const SettingsNavigation = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image style={styles.topimg} source={require('./assets/UI_design_top.png')} />
            <View style={styles.title}>
                <Text style={styles.titletext}>Settings!</Text>
            </View>
            <Image style={styles.bottomimg} source={require('./assets/UI_design_bottom.png')} />
            <View style={{ backgroundColor: "#F2F2F2", height: 510, }}>
                <TouchableOpacity style={{ backgroundColor: "#FFF", height: 80, marginVertical: 5, marginTop: 10, }}>
                    <View style={{ flexDirection: "row" }}>
                        <Image style={[styles.navicon, { marginLeft: 20, marginTop: 15, width: 50, height: 50 }]} source={require('./assets/lock.png')} />
                        <View style={{ marginTop: 21, marginLeft: 10, }}>
                            <Text style={{ fontWeight: "bold" }}>Security</Text>
                            <Text>Manage account security</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: "#FFF", height: 80, marginVertical: 5 }}>
                    <View style={{ flexDirection: "row" }}>
                        <Image style={[styles.navicon, { marginLeft: 20, marginTop: 15, width: 50, height: 50 }]} source={require('./assets/shield.png')} />
                        <View style={{ marginTop: 21, marginLeft: 10, }}>
                            <Text style={{ fontWeight: "bold" }}>Protect</Text>
                            <Text>Manage account protection</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: "#FFF", height: 80, marginVertical: 5 }}
                    onPress={() => navigation.navigate('API')}>
                    <View style={{ flexDirection: "row" }}>
                        <Image style={[styles.navicon, { marginLeft: 20, marginTop: 15, width: 50, height: 50 }]} source={require('./assets/otheropt.png')} />
                        <View style={{ marginTop: 21, marginLeft: 10, }}>
                            <Text style={{ fontWeight: "bold" }}>API</Text>
                            <Text>Load API Implementation</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: "#FFF", height: 80, marginVertical: 5 }}>
                    <View style={{ flexDirection: "row" }}>
                        <Image style={[styles.navicon, { marginLeft: 20, marginTop: 15, width: 50, height: 50 }]} source={require('./assets/share.png')} />
                        <View style={{ marginTop: 21, marginLeft: 10, }}>
                            <Text style={{ fontWeight: "bold" }}>Logout</Text>
                            <Text>Logout Account</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        </View >

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        height: windowHeight,
        width: windowWidth,
    },
    topimg: {
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginTop: 0,
    },
    bottomimg: {
        position: "absolute",
        bottom: 0,
        justifyContent: "flex-end",
        alignSelf: "flex-end",
    },
    title: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    titletext: {
        fontSize: 40,
        fontFamily: 'GlacialIndifferenceBold',
        color: "#02abff",
        marginBottom: 0,
        width: 200,
        height: 60,
        textAlign: "center",

        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: 2, height: 1 },
        textShadowRadius: 7,
    },
});

export default Settings;