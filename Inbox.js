import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, Dimensions } from 'react-native';



const Inbox = (props) => {

  return (
    <View style={styles.container}>
      <Image style={styles.topimg} source={require('./assets/UI_design_top.png')} />
      <Text style={{ textAlign: "center", marginTop: 50, marginBottom: 20, fontSize: 30, fontWeight: "bold" }}>INBOX</Text>
      <View style={styles.box}>
        <Text style={{ paddingTop: 10, paddingLeft: 10, paddingBottom: 10 }}>TODAY                                        Mark all read     Sort by Time</Text>
        <View style={styles.inner}>
          <Image style={styles.img} source={require('./assets/christian.jpg')} />
          <Text style={{ marginTop: 30, fontWeight: "bold", }}>Christian Angelo Navera{"\n"}Kailan ka gagawa ng ano..</Text>
          <Text style={{ marginTop: 30, marginLeft: 50, }}>{"\n"}3m ago</Text>
        </View>
        <View style={styles.inner}>
          <Image style={styles.img} source={require('./assets/fred.jpg')} />
          <Text style={{ marginTop: 30, fontWeight: "bold", }}>Frederick Marlang{"\n"}Tara laro?</Text>
          <Text style={{ marginTop: 30, marginLeft: 95, }}>{"\n"}10m ago</Text>
        </View>
        <View style={styles.inner}>
          <Image style={styles.img} source={require('./assets/sai.jpg')} />
          <Text style={{ marginTop: 30, fontWeight: "bold", }}>Saira Caga{"\n"}Tara gym nalang tayo</Text>
          <Text style={{ marginTop: 30, marginLeft: 80, }}>{"\n"}19m ago</Text>
        </View>
      </View>
      <View style={styles.navbar}>
        <View style={styles.navcontainer}>
          <TouchableOpacity style={styles.navbutton}>
            <Image style={styles.navicon} source={require('./assets/home_or_newsfeed.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.navbutton}>
            <Image style={styles.navicon} source={require('./assets/new_notification.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.navbutton}>
            <Image style={styles.navicon} source={require('./assets/create_post_button.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.navbutton}>
            <Image style={styles.navicon} source={require('./assets/new_message.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.navbutton}>
            <Image style={styles.navicon} source={require('./assets/search_icon.png')} />
          </TouchableOpacity>
        </View>
      </View>
    </View>

  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },

  header: {
    flexDirection: 'column',
    backgroundColor: "#FFFF",
    height: '20%',
    width: '100%',
  },

  container: {
    width: '100%',
    height: '25%',
    padding: 3,
    flexDirection: "row",
    flexWrap: 'wrap',
  },

  topimg: {
    marginTop: -3,
    marginLeft: -3,
  },

  box: {
    height: 500,
    backgroundColor: "#f2f2f2",
  },

  inner: {
    flexDirection: "row",
  },

  img: {
    width: 60,
    height: 60,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 10,
    resizeMode: 'cover',
    borderWidth: 2,
    borderRadius: 75,
  },
  navbar: {
    position: "relative",
    marginTop: 60,


  },
  navcontainer: {
    backgroundColor: "rgba(255, 255, 255, 0.3)",
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  navbutton: {
    alignItems: "center",
    justifyContent: "center",
  },
  navicon: {
    width: 35,
    height: 35,
    marginHorizontal: 20,
  },

});
export default Inbox;